<?php

$data->name = 'Suci Ramadini';
$data->age = 24;
$data->address = 'Jln. DI. Panjaitan, Gg. Gaya Baru No.626 Palembang - Sumatera Selatan 30265';
$data->hobbies = array('Drink Coffee', 'Watch Movies', 'listen to song');
$data->married = false;
$data->school[] = array([
	'Name' => 'SDN 249 Palembang',
	'year in' => 2002,
	'year out' => 2008,
	'major' => null],
	[
	'Name' => 'SMPN 30 Palembang',
	'year in' => 2008,
	'year out' => 2011,
	'major' => null,
	],
	[
	'Name' => 'SMAN 8 Palembang',
	'year in' => 2011,
	'year out' => 2014,
	'major' => 'IPA',
	],
	[
	'Name' => 'Universitas Bina Darma Palembang',
	'year in' => 2014,
	'year out' => 2019,
	'major' => 'Teknik Informatika',
	]
);
$data->Skill[]= array(
	[
		'Name' => 'PHP',
		'Level' => 'Beginner'
	],
	[
		'Name' => 'Laravel',
		'Level' => 'Beginner'
	],
	[
		'Name' => 'CSS',
		'Level' => 'Beginner'
	]
);
$data->interest_in_coding=true;

$mydata = json_encode($data);
echo($mydata);
// Mengencode data menjadi json
// $mydata = json_encode($data, JSON_PRETTY_PRINT);

// Menyimpan data ke dalam anggota.json
file_put_contents('anggota.json', $mydata);
?>